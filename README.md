# Women in Tech Social!

* Amanda Kiehm
* Allisha Rectspat
* Starr Solis
* Rachael Schlosberg
* Cise Babatasi

The social media application for women interested in tech to connect and share ideas.

## Intended market

The people that we expect to use this are
women/girls and gender expansive individuals who are involved or interested in the tech industry. 
These may include (not limited)

* Companies looking to hire
* Tech conference, meet-up or event organisers or participants
* Job seekers
* Tech industry influencers
* Tech mentors/mentees
* Businesses, educational institutions and non-profit organizations

## Functionality

* Live feed
* Job board
* Profile sign-up / log-in
* Conference, meet-up and event board
* Search feature(event, profile, conference, jobs, companies, location, time) or sort by a tags or newest to oldest
* Direct message
* Forum page & comments (annonymous comments will be available)
* Tips - blog, 
* Mentorship / sponsorship / scholarship
* Newsletter
* Following/subscription (number of followers will be hidden to public)
* Company info & reviews
